﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Input;
using BmMathLib;
using wpf_calc_beta.Code;

// ReSharper disable UnusedMember.Global

namespace wpf_calc_beta
{
    /// <summary>
    /// Interaction logic for CalcBeta.xaml
    /// </summary>
    public partial class CalcBeta
    {

        private BmMathLib.Calc calc;
        private string ExpressionText
        {
            get => MathTb.Text;
            set => MathTb.Text = value;
        }

        private string ResultText
        {
            get => ResultTb.Text;
            set => ResultTb.Text = "=" + value;
        }

        public CalcBeta() //Инициализация калькулятора
        {
            InitializeComponent();
            calc = new BmMathLib.Calc();
            ExpressionText = "0";
            ResultText = "0";
            TextBoxes.RowDefinitions[1].Height = new GridLength(0.0, GridUnitType.Star); // Прячем результат
        }

        private void ButtonPressed(string sender)
        {
            if ("0123456789".Contains(sender))
            {
                calc.AddNumber(sender);
            }
            // Если не число
            switch (sender)
            {
                case (MathStuff.Plus):
                case (MathStuff.Minus):
                case (MathStuff.Div):
                case (MathStuff.Mult):
                    calc.AddAction(sender);
                    break;
                case (MathStuff.Sine):
                case (MathStuff.Cosine):
                case (MathStuff.Tangent):
                case (MathStuff.Cotangent):
                case (MathStuff.Lg):
                case (MathStuff.Log):
                case (MathStuff.Ln):
                    calc.AddFunction(sender);
                    break;
                case (MathStuff.Root):
                    calc.AddSqrt();
                    break;
                case (MathStuff.Pow):
                    calc.AddPow();
                    break;
                case (MathStuff.Fact):
                    calc.AddFactorial();
                    break;
                case (MathStuff.Comma):
                case (MathStuff.Deg):
                    calc.AddSpec(sender);
                    break;
                case (MathStuff.Pi):
                case (MathStuff.Exp):
                    calc.AddConstant(sender);
                    break;
                case (MathStuff.Dot):
                    calc.AddDot(sender);
                    break;
                case ("("):
                case (")"):
                    calc.AddBracket(sender);
                    break;
                case ("r"):
                    calc.RemoveOneSymbolFromEndOfExpression();
                    break;
                case ("c"):
                    calc.ClearExpression();
                    break;
                case (MathStuff.Equal):
                    if (ResultText.Contains("Error") ||
                        ResultText.Contains(double.PositiveInfinity.ToString(CultureInfo.CurrentCulture)))
                    {
                        calc.ClearExpression();
                        break;
                    }

                    calc = new Calc(ResultText.Remove(0, 1));
                    break;
            }

            ExpressionText = calc.GetExpressinText();
        }

        private void WriteToResult()
        {
            var result = calc.EvaluateExpression();
            TextBoxes.RowDefinitions[1].Height = result == calc.GetEvaluateableExpression() ||
                                                 result == string.Empty
                ? new GridLength(0.0, GridUnitType.Star)
                : new GridLength(30.0, GridUnitType.Star);
            if (!result.Contains("Error"))
                ResultText = result;
        }

        private void ButtonClick(object sender, RoutedEventArgs e)
        {
            ButtonPressed(ButtonClickHandler.HandleSender(sender));
            WriteToResult();
        }

        private void FunctionShowClick(object sender, RoutedEventArgs e)
        {
            CalcGrid.RowDefinitions[2].Height =
                CalcGrid.RowDefinitions[2].Height == new GridLength(30, GridUnitType.Star)
                    ? new GridLength(0, GridUnitType.Star)
                    : new GridLength(30, GridUnitType.Star);
        }

        private void KeyboardPress(object sender, KeyEventArgs e)
        {
            ButtonPressed(KeyboardModule.KeyClickHandle(e.Key));
            WriteToResult();
        }
    }
}