﻿using System.Windows.Input;
using BmMathLib;

namespace wpf_calc_beta.Code
{
    public static class KeyboardModule
    {
        public static string KeyClickHandle(Key input)
        {
            var mathString = string.Empty;
            switch (input)
            {
                //Числа
                case (Key.D1):
                case (Key.NumPad1):
                    if (Keyboard.IsKeyDown(Key.LeftShift))
                    {
                        mathString = "!";
                        break;
                    }

                    mathString = "1";
                    break;
                case (Key.D2):
                case (Key.NumPad2):
                    mathString = "2";
                    break;
                case (Key.D3):
                case (Key.NumPad3):
                    mathString = "3";
                    break;
                case (Key.D4):
                case (Key.NumPad4):
                    mathString = "4";
                    break;
                case (Key.D5):
                case (Key.NumPad5):
                    mathString = "5";
                    break;
                case (Key.D6):
                case (Key.NumPad6):
                    if (Keyboard.IsKeyDown(Key.LeftShift))
                    {
                        mathString = "^";
                        break;
                    }

                    mathString = "6";
                    break;
                case (Key.D7):
                case (Key.NumPad7):
                    mathString = "7";
                    break;
                case (Key.D8):
                case (Key.NumPad8):
                    if (Keyboard.IsKeyDown(Key.LeftShift))
                    {
                        mathString = MathStuff.Mult;
                        break;
                    }

                    mathString = "8";
                    break;
                case (Key.D9):
                case (Key.NumPad9):
                    if (Keyboard.IsKeyDown(Key.LeftShift))
                    {
                        mathString = "(";
                        break;
                    }

                    mathString = "9";
                    break;
                case (Key.D0):
                case (Key.NumPad0):
                    if (Keyboard.IsKeyDown(Key.LeftShift))
                    {
                        mathString = ")";
                        break;
                    }

                    mathString = "0";
                    break;
                //Действия
                case (Key.OemPlus):
                case (Key.Enter):
                    if (Keyboard.IsKeyDown(Key.LeftShift) && input == Key.OemPlus)
                    {
                        mathString = MathStuff.Plus;
                        break;
                    }

                    mathString = MathStuff.Equal;
                    break;
                case (Key.OemMinus):
                case (Key.Subtract):
                    mathString = MathStuff.Minus;
                    break;
                case (Key.Back):
                    mathString = "r";
                    break;
                case (Key.Divide):
                    mathString = MathStuff.Div;
                    break;
                case (Key.Multiply):
                    mathString = MathStuff.Mult;
                    break;
                case (Key.Add):
                    mathString = MathStuff.Plus;
                    break;
                case (Key.Decimal):
                    mathString = MathStuff.Dot;
                    break;
            }

            return mathString;
        }
    }
}