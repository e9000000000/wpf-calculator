﻿using System;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using org.mariuszgromada.math.mxparser;

namespace BmMathLib
{
    public class Calc
    {
        private Expression expression;
        public Calc()
        {
            expression = new Expression();
        }
        public Calc(string expression)
        {
            this.expression = new Expression(expression);
        }

        public string GetExpressinText() => expression.ToString();

        public string EvaluateExpression()
        {
            return expression.Evaluate();
        }

        public string GetEvaluateableExpression()
        {
            return expression.ToEvaluateableString();
        }

        public void AddNumber(string num)
        {
            if (expression.IsCleared())
                expression.RemoveLastSymbol();
            if (expression.IsMultNeed())
                expression.Append(MathStuff.Mult); //Ставится умножение при определенных условиях

            expression.Append(num);
        }

        public void AddAction(string act)
        {
            #region MinusAfterSymbols

            if (expression.IsMatchToRe(@"[\(÷×\^]$") && act == MathStuff.Minus)
            {
                expression.Append(act);
                return; //Тут короче код отвечающий за минус после открывающейся скобки
            }

            if (expression.IsMatchToRe(@"(\(\-|\^\-|×\-|÷\-)$") && act == MathStuff.Plus)
            {//Если нажали плюс, то минус после скобки убирается
                expression.RemoveLastSymbol();
                return;
            }

            if (expression.IsMatchToRe(@"(\(\-|\^\-|×\-|÷\-)$"))
                return; //на случай чтобы не получить вместо минуса любое другое действие

            #endregion

            switch (expression.ToString())
            {
                case "0" when act == MathStuff.Minus:
                    expression.ReplaceLastSymbol(MathStuff.Minus);
                    break;
                case MathStuff.Minus when act == MathStuff.Plus:
                    return;
                case MathStuff.Minus when act == MathStuff.Mult || act == MathStuff.Div:
                    return;
            }

            if (expression.IsMatchToRe(@"[,\(]$"))
                return;
            if (expression.IsMatchToRe(@"[÷×\^\+\-\.]$"))
                expression.RemoveLastSymbol();

            expression.Append(act);
        }

        public void AddDot(string dot)
        {
            if (!expression.IsMatchToRe(@"(\.[0-9]*|[^0-9])$"))
                expression.Append(dot);
        }

        public void AddBracket(string bracket)
        {
            //Проверка чтобы не ставили закрывающую скобку после действия и открывающей скобки
            if (expression.GetMissingClosingBracketsQuantity() <= 0 && bracket == ")") return;
            if (expression.IsMatchToRe(@"[÷×\^\+\-\(]$") && bracket == ")") return;

            //Если перед скобкой точка, или весь пример это 0, то убираем точку/ноль
            if (expression.EndsWith(MathStuff.Dot) ||
                expression.IsCleared()) expression.RemoveLastSymbol();

            //Если после числа нет действий то ставим умножение
            if (expression.IsMatchToRe(@"[0-9\)πe°]$") && bracket == "(")
                expression.Append(MathStuff.Mult);

            expression.Append(bracket);
        }

        public void AddPow()
        {
            if (expression.IsEndsWithAction() || expression.EndsWith("("))
                return;
            if (expression.IsEndsWithAction() || expression.EndsWith(MathStuff.Dot))
                expression.RemoveLastSymbol();

            expression.Append(MathStuff.Pow);
        }

        public void AddFactorial()
        {
            if (expression.IsMatchToRe(@"[0-9πe\)]$"))
                expression.Append(MathStuff.Fact);
        }

        public void AddSqrt()
        {
            if (expression.IsCleared() || expression.EndsWith(MathStuff.Dot)) expression.RemoveLastSymbol();
            //Тут проверка чтобы знак умножения ставился сам в определенных случаях
            if (expression.IsEndsWithNumber() || expression.IsMultNeed())
                expression.Append(MathStuff.Mult);

            expression.Append(MathStuff.Root);
        }

        public void AddConstant(string cnst)
        {
            if (expression.IsCleared() || expression.EndsWith(MathStuff.Dot))
                expression.RemoveLastSymbol();
            if (expression.IsEndsWithNumber() || expression.IsMultNeed())
                expression.Append(MathStuff.Mult);

            expression.Append(cnst);
        }

        public void AddFunction(string func)
        {
            if (expression.IsCleared() || expression.EndsWith(MathStuff.Dot))
                expression.RemoveLastSymbol();
            if (expression.IsEndsWithNumber() || expression.IsMultNeed())
                expression.Append(MathStuff.Mult);

            expression.Append(func);
        }

        public void AddSpec(string spec)
        {
            if (expression.IsMatchToRe(@"[0-9πe\),°]$") && !expression.EndsWith(spec))
                expression.Append(spec);
        }

        public void RemoveOneSymbolFromEndOfExpression()
        {
            expression.RemoveLastSymbol();
            if (expression.IsEmpty())
                expression.Append("0");
        }

        public void ClearExpression()
        {
            expression.Clear();
        }

    }
}