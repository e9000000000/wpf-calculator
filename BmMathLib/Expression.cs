﻿using System;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using org.mariuszgromada.math.mxparser;

namespace BmMathLib
{
    public class Expression
    {

        private string expression;
        public Expression()
        {
            expression = "0";
        }
        public Expression(string expression)
        {
            this.expression = expression;
        }

        public override string ToString() => expression;
        public void Append(string str)
        {
            expression = expression + str;
        }

        public void Clear()
        {
            expression = "0";
        }
        public void RemoveLastSymbol()
        {
            expression = RemoveLastSymbol(expression);
        }
        private static string RemoveLastSymbol(string str)
        {
            int last_symbol_len = LastSymbolLength(str);
            return str.Remove(str.Length - last_symbol_len, last_symbol_len);
        }
        public void ReplaceLastSymbol(string newStr)
        {

            RemoveLastSymbol();
            Append(newStr);
        }

        public bool IsEmpty() => expression == string.Empty;
        public bool IsCleared() => expression == "0";

        public bool EndsWith(string str)
        {
            return expression.EndsWith(str);
        }

        public bool IsMatchToRe(string re)
        {
            return new Regex(re).IsMatch(expression);
        }

        public bool IsEndsWithAction() => IsMatchToRe(@"[\+\^\-×÷]$");

        public int LastSymbolLenght()
        {
            return LastSymbolLength(expression);
        }
        private static int LastSymbolLength(string str)
        {
            if (str.Length == 0)
                return 0;

            if (str.EndsWith(MathStuff.Sine) ||
                str.EndsWith(MathStuff.Cosine) ||
                str.EndsWith(MathStuff.Tangent) ||
                str.EndsWith(MathStuff.Cotangent) ||
                str.EndsWith(MathStuff.Log))
                return 4;

            if (str.EndsWith(MathStuff.Lg) ||
                str.EndsWith(MathStuff.Ln))
                return 3;

            if (str.EndsWith(MathStuff.Root) ||
                str.Length == 2 &&
                str[0] == '-')
                return 2;

            return 1;
        }

        public bool IsMultNeed() => EndsWith(")") ||
                                    EndsWith(MathStuff.Pi) ||
                                    EndsWith(MathStuff.Exp) ||
                                    EndsWith(MathStuff.Deg) ||
                                    EndsWith(MathStuff.Fact);

        public bool IsEndsWithNumber()
        {
            return IsEndsWithNumber(expression);
        }

        private static bool IsEndsWithNumber(string expr)
        {
            return expr != string.Empty && int.TryParse(expr[expr.Length - 1].ToString(), out _);
        }
        public string ToEvaluateableString()
        {
            string result = expression;
            while (!IsEndsWithNumber(result) &&
                   !result.EndsWith(")") &&
                   !result.EndsWith(MathStuff.Fact) &&
                   !result.EndsWith(MathStuff.Deg) &&
                   !result.EndsWith(MathStuff.Pi) &&
                   !result.EndsWith(MathStuff.Exp))
            {
                if (result == string.Empty) return "0";
                result = RemoveLastSymbol(result);
            }

            return PlaceBrackets(result);
        }

        private static string ReplaceActionsToMxParserFormat(string expr)
        {
            return expr.Replace(MathStuff.Mult, "*")
                       .Replace(MathStuff.Div, "/")
                       .Replace(MathStuff.Root, "sqrt(")
                       .Replace(MathStuff.Pi, "pi")
                       .Replace(MathStuff.Deg, "*[deg]")
                       .Replace(MathStuff.Lg, "log10(");
        }

        private string ToEvaluateable()
        {
            return ReplaceActionsToMxParserFormat(this.ToEvaluateableString());
        }

        public string Evaluate() //Вычисление
        {
            double resultDouble;
            string mxparserExpression = this.ToEvaluateable();
            try
            {
                resultDouble = new org.mariuszgromada.math.mxparser.Expression(mxparserExpression).calculate();
            }
            catch (DivideByZeroException)
            {
                resultDouble = double.NaN;
            }
            catch (OverflowException)
            {
                resultDouble = double.PositiveInfinity;
            }

            if (!double.IsNaN(resultDouble))
            {
                return resultDouble.ToString(CultureInfo.CurrentCulture)
                                   .Replace(MathStuff.Comma, MathStuff.Dot);
            }
            return "Error";
        }

        public int GetMissingClosingBracketsQuantity()
        {
            return GetMissingClosingBracketsQuantity(expression);
        }
        private static int GetMissingClosingBracketsQuantity(string expr) //Получение числа скобок
        {
            var openBracketsQuantity = expr.Length - expr.Replace("(", string.Empty).Length; //Кол-во открывающихся скобок
            var closeBracketsQuantity = expr.Length - expr.Replace(")", string.Empty).Length; //Кол-во закрывающихся скобок

            return openBracketsQuantity - closeBracketsQuantity;
        }

        private static string PlaceBrackets(string expr) //Добавление нехватающих скобок
        {
            var brAmt = GetMissingClosingBracketsQuantity(expr);
            expr += string.Concat(Enumerable.Repeat(")", brAmt));
            return expr;
        }
    }
}